package rw.ac.rca.termOneExam.utils;

public class Utility {
    public static double convertCelciusToFahrenheit(double celcius) {
        return (9/5) * celcius + 32;
    }
}
