package rw.ac.rca.termOneExam.controller;


import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.utils.APICustomResponse;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityControllerIntegrationTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getAll_Success() throws JSONException {
        String response = this.restTemplate.getForObject("/api/cities/all", String.class);
        System.out.println(response);
        JSONAssert.assertEquals("[{id:101},{id:102},{id:103},{id:104}]", response, false);
    }


    @Test
    public void getById_Success() {
        ResponseEntity<City> responseEntity = this.restTemplate.getForEntity("/api/cities/id/101", City.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(101, responseEntity.getBody().getId());
    }

    @Test
    public void getById_NotFound() {
        ResponseEntity<APICustomResponse> responseEntity = this.restTemplate.getForEntity("/api/cities/id/23", APICustomResponse.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertFalse(responseEntity.getBody().isStatus());
        assertEquals("City not found with id 23", responseEntity.getBody().getMessage());

    }

    @Test
    public void saveCity_Success() {
        CreateCityDTO dto = new CreateCityDTO();
        dto.setName("Nyabihu");
        dto.setWeather(14);

        ResponseEntity<City> responseEntity = this.restTemplate.postForEntity("/api/cities/add", dto, City.class);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals("Nyabihu", responseEntity.getBody().getName());
    }

    @Test
    public void saveCity_NameExists() {
        CreateCityDTO dto = new CreateCityDTO();
        dto.setName("Musanze");
        dto.setWeather(19);

        ResponseEntity<APICustomResponse> responseEntity = this.restTemplate.postForEntity("/api/cities/add", dto, APICustomResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertFalse(responseEntity.getBody().isStatus());
        assertEquals("City name Musanze is registered already", responseEntity.getBody().getMessage());

    }

}
