package rw.ac.rca.termOneExam.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.repository.ICityRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;



@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @Mock
    private ICityRepository cityRepositoryMock;

    @InjectMocks
    private CityService cityService;

    @Test
    public void getAll_withData() {
        when(cityRepositoryMock.findAll())
                .thenReturn(
                        Collections.singletonList(
                                new City(105, "Nyabihu", 20, 68)
                        )
                );
        assertEquals(105, cityService.getAll().get(0).getId());
        assertEquals(20, cityService.getAll().get(0).getWeather());
    }

    @Test
    public void getAll_noData() {
        when(cityRepositoryMock.findAll())
                .thenReturn(new ArrayList<>());
        assertTrue(cityService.getAll().isEmpty());
        assertEquals(0, cityService.getAll().size());

    }

    @Test
    public void getById_Success() {
        City city = new  City(105, "Nyabihu", 20, 68);

        when(cityRepositoryMock.findById(105L)).thenReturn(Optional.of(city));

        assertEquals(105, cityService.getById(105).get().getId());
        assertEquals("Nyabihu", cityService.getById(105).get().getName());
    }

    @Test
    public void getById_NotFound() {
        City city = new  City(105, "Nyabihu", 20, 68);

        when(cityRepositoryMock.findById(105L)).thenReturn(Optional.of(city));

        assertFalse(cityService.getById(3).isPresent());
    }

    @Test
    public void createCity_Success() {

        CreateCityDTO dto = new CreateCityDTO();
        dto.setName("Busogo");
        dto.setWeather(22);

        when(cityRepositoryMock.save(any(City.class))).thenReturn(new City(dto.getName(), dto.getWeather()));

        assertEquals("Busogo", cityService.save(dto).getName());
        assertEquals(22, cityService.save(dto).getWeather());
    }


    @Test
    public void createCity_Failure() {

        CreateCityDTO dto = new CreateCityDTO();
        dto.setName("Musanze");
        dto.setWeather(22);

        when(cityRepositoryMock.existsByName(dto.getName())).thenReturn(true);

        assertTrue(cityService.existsByName(dto.getName()));
    }


}
