package rw.ac.rca.termOneExam.utils;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.repository.ICityRepository;
import rw.ac.rca.termOneExam.service.CityService;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@RunWith(SpringRunner.class)
public class CityUtilTest {


    @Autowired
    private ICityRepository cityRepository;

    @Test
    public void cityWithWeatherMoreThan40_Test(){
        boolean exists = false;
        List<City> cities = cityRepository.findAll();

        for (City city: cities) {
            exists = city.getWeather() > 40;
        }

        assertFalse(exists);
    }

    @Test
    public void cityWithWeatherLessThan10_Test(){
        boolean exists = false;
        List<City> cities = cityRepository.findAll();

        for (City city: cities) {
            exists = city.getWeather() < 10;
        }

        assertFalse(exists);
    }

    @Test
    public void citiesContainMusanzeAndKigali_Test(){
        assertTrue(cityRepository.existsByName("Kigali"));
        assertTrue(cityRepository.existsByName("Musanze"));
    }




    @RunWith(MockitoJUnitRunner.class)
    public static class SpyTest {

        @Spy
        private ICityRepository cityRepositoryBySpy;

        @InjectMocks
        private CityService cityService;

        @Test
        public void testSpying() {
            when(cityRepositoryBySpy.findAll()).thenReturn(Arrays.asList(new City("Mukamira", 19)));

            List<City> cities = cityService.getAll();

            assertEquals(19, cities.get(0).getWeather());
            assertFalse(cities.isEmpty());
        }

    }


        @RunWith(MockitoJUnitRunner.class)
    public static class MockTest {

        @Mock
        private ICityRepository cityRepositoryByMock;

        @InjectMocks
        private CityService cityService;

        @Test
        public void testMocking(){
            when(cityRepositoryByMock.findAll()).thenReturn(Arrays.asList(new City("Mukamira", 19)));

            List<City> cities = cityService.getAll();

            assertEquals("Mukamira", cities.get(0).getName());
        }

    }
}
